#!/bin/bash

set -eu

# Ensure that data directory is owned by 'cloudron' user
chown -R cloudron:cloudron /app/data
touch /app/data/.env

# Path to .env file
ENV_FILE="/app/data/.env"

# Function to append variable if not exists
append_if_not_exists() {
    local key="$1"
    local value="$2"

    # Check if the key already exists in the file
#    if ! grep -q "^${key}=" "$ENV_FILE"; then
#        echo "${key} = ${value}" >> "$ENV_FILE"
#    fi
    if ! grep -q "^${key} =" "$ENV_FILE"; then
    echo "${key} = ${value}" >> "$ENV_FILE"
    fi
}

# Append custom environment variables to existing .env file
append_if_not_exists "BASE_URL" "https://${CLOUDRON_APP_DOMAIN}/"

append_if_not_exists "DB" "mongodb://${CLOUDRON_MONGODB_USERNAME}:${CLOUDRON_MONGODB_PASSWORD}@${CLOUDRON_MONGODB_HOST}:${CLOUDRON_MONGODB_PORT}/${CLOUDRON_MONGODB_DATABASE}"

append_if_not_exists "PORT" "3000"
append_if_not_exists "COOKIE_KEY" "4c5h3sve45yte55rgae984hvs4780gtsRH5ygrs54r5Hr4adRSjgg66y8634"
append_if_not_exists "SIGNUPS" "on"
append_if_not_exists "NODE_OPTIONS" "--max_old_space_size=460"
append_if_not_exists "HTTPS" "true"
append_if_not_exists "APPDIRECTORY" "'voip'"

echo "Starting Node.js app"

# Run the app as user 'cloudron'
exec /usr/local/bin/gosu cloudron:cloudron node /app/code/VoIP/app.js
