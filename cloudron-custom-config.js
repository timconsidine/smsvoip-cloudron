const dotenv = require('dotenv');
const path = require('path');

dotenv.config({
    path: '/app/data/.env'
});

module.exports = {
    NODE_ENV : process.env.NODE_ENV || 'production',
    HOST : process.env.HOST || 'localhost',
    PORT : process.env.PORT || 3000
}

console.log("Environment Variables: ", process.env);
