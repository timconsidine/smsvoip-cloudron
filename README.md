# smsvoip-cloudron

This is a self-packaged app for Cloudron, based on https://github.com/0perationPrivacy/VoIP

## Why : App Description
Sometimes you need a 'virtual' number which is not tied to a mobile device, e.g. phone.
This app allows you to do SMS from your desktop via web browser interface.

My first usage of this app was to send and receive SMS while I was in a non-EU country (no free roaming included).

Additionally it can be used to register a mobile number for use in WhatsApp, Signal, Telegram or Facebook (FB not tested : why would I want to be on FB?!).

You can then use such apps on your phone without sharing/disclosing/registering your real mobile number.

This allows you to minimise (avoid) spam calls, e.g. when a **scammer** or **spammer** harvests your number from a WhatsApp group.

## Why : Cloudron

- because Clouron is a BRILLIANT self-hosting environment, which handles backups etc. much better than other PaaS (platform as a service) systems.
- because the app requires MongoDB and this is included in a Cloudron instance (no additional installtion / configuration required)

## Requirements

- already installed Cloudron on your VPS or a home server 
- Docker installed on your development device
- Docker repository available (on Docker hub or your private Docker repo on Cloudron)
- Cloudron CLI installed on your development device and logged in
- purchased (rented) a mobile number from Telnyx or Twilio

## Installation

- clone this repo to your development device
- `cd` into the directory
- manual process : 
  - build the Dockerfile
  - upload the Docker image to your Docker repo
  - install to your Cloudron instance
- scripted process :
  - run `./cld.sh` to do thiose 3 steps 
  - e.g. `./cld.sh your-docker-repo/smsvoip-cloudron:tag`
  - the script will prompt you for the `Location' : this is the subdomain for the app
  - e.g. `smsvoip.domain.tld`

## Configuration

On launch, this package will create a configuration file `/app/data/.env` with the necessary values.

- Signup and login.
- Add a `Profile`.
- Configure the profile with your API key (in Telnyx portal) and the number from Telnyx.

I use Telnyx, not Twilio, so I don't have instructions for that, but they're (probably) broadly similar.

Full instructions at https://inteltechniques.com/voip.suite.html but the majority of thst page is about how to set up a free Heroku-hosted installation. **SCROLL DOWN** to where it describes how to add a Twilio or Telnyx number.

**NB :** Telnyx portal has lots of settings, including attaching an app.
**YOU DO NOT NEED TO TO ATTACH AN APP**
This app will automatically configure the number in Telnyx after you provide an API key.

## Disable Signups

Initially the app allows a signup to create an account.
But you probably don't want other people to do so, because they can see your message and use it for their own SMS.  This could lead to high charges from Telnyx/Twilio.

So after creating your account, **IT IS HIGHLY RECOMMENDED** to disable signups.

- In a Terminal session or the Cloudron File Manager, edit the file at `/app/data/.env`.
- Change SIGNUPS to be off.
- Save the file.
- Reload your app login page.
- Check that the signup option is not shown.

## Next steps

- add screenshots
- check voice usage (not just SMS)
- create a 'pure' Docker installation (so the app can be deployed on any VPS (not just Cloudron)
