FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

RUN mkdir -p /app/code /app/data
WORKDIR /app/code
RUN git clone https://github.com/0perationPrivacy/VoIP/ 

ADD start.sh /app/code/
# Overwrite config.js
COPY cloudron-custom-config.js /app/code/VoIP/config.js
RUN rm /app/code/VoIP/.env

#COPY cloudron-custom.env /app/data/.env

# install packages
WORKDIR /app/code/VoIP
RUN npm install

CMD [ "/app/code/start.sh" ]
